#Mutli Social Share
####Speed, Simplicity, Security
#![](https://www.multisocialshare.com/favicon.ico)
####Download

* [Download from Google Play](https://play.google.com/store/apps/details?id=com.kbsbng.androidapps.MultiSocialShare)

####Features
* History

* Follows Google design guidelines

* Unique utilization of navigation drawer for tabs

####Permissions

* ````INTERNET````: For accessing the web

* ````WRITE_EXTERNAL_STORAGE````: For downloading files from the browser

* ````READ_EXTERNAL_STORAGE````: For downloading files from the browser

* ````ACCESS_NETWORK_STATE````: Required for the WebView to function by some OEM versions of WebKit

####The Code
* Please contribute code back if you can. The code isn't perfect.
* Please add translations/translation fixes as you see need

####Contributing
* Contributions are always welcome
* If you want a feature and can code, feel free to fork and add the change yourself and make a pull request

####License
````
Copyright 2015 Keshavaprasad B S

Multi Social Share using Lightning Browser

   This Source Code Form is subject to the terms of the 
   Mozilla Public License, v. 2.0. If a copy of the MPL 
   was not distributed with this file, You can obtain one at 
   
   http://mozilla.org/MPL/2.0/
````
This means that you MUST provide attribution in your application to Multi Social Share with
Lightning Browser for the use of this code. The way you can do this is to provide a separate screen
in settings showing what open-source libraries and/or apps (this one) you used in your application.
You must also open-source any files that you use from this repository and if you use any code at all
from this repository, the file you put it in must be open-sourced according the the MPL 2.0 license.
To put it simply, if you create a fork of this browser, your browser must be open-source, no
exceptions. The only way to avoid open-sourcing a file is to completely write all the code yourself
and to not use any code from Multi Social Share with Lightning. This is in order to provide a way
for companies to utilize the code without making private server code public.
