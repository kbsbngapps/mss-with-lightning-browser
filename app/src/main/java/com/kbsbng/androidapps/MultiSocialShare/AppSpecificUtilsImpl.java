package com.kbsbng.androidapps.MultiSocialShare;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import com.kbsbng.androidapps.common.AppActivity;
import com.kbsbng.androidapps.common.AppActivityI;
import com.kbsbng.androidapps.common.AppSpecificUtils;

@SuppressWarnings("unused")
public class AppSpecificUtilsImpl implements AppSpecificUtils{
    @Override
    public String getTosUrl(Context context) {
        return null;
    }

    @Override
    public void addCommmonMenuOptions(Activity context, Menu menu) {

    }

    @Override
    public <T extends Activity & AppActivityI> void onActivityResult(T context, int requestCode, int resultCode, Intent data) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item, Activity activity) {
        return false;
    }

    @Override
    public int getRemoveAdMenuItem(Activity activity) {
        return -1;
    }
}
