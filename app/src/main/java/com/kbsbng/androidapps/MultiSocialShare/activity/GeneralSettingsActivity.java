/*
 * Copyright 2014 A.C.R. Development
 */
package com.kbsbng.androidapps.MultiSocialShare.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.kbsbng.androidapps.MultiSocialShare.R;
import com.kbsbng.androidapps.MultiSocialShare.constant.Constants;
import com.kbsbng.androidapps.MultiSocialShare.preference.PreferenceManager;
import com.kbsbng.androidapps.MultiSocialShare.utils.Utils;

public class GeneralSettingsActivity extends ThemableSettingsActivity {

	// mPreferences variables
	private PreferenceManager mPreferences;
	private int mAgentChoice;
	private TextView mAgentTextView;
	private TextView mDownloadTextView;
	private String mDownloadLocation;
	private Activity mActivity;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.general_settings);

		// set up ActionBar
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//		setSupportActionBar(toolbar);

//		final ActionBar supportActionBar = getSupportActionBar();
//		if (supportActionBar != null) {
//			supportActionBar.setDisplayHomeAsUpEnabled(true);
//		}
		setActionBar(toolbar);

		mPreferences = PreferenceManager.getInstance();

		mActivity = this;
		initialize();
	}

	@Override
	public String getAdmobAppId() {
		return null;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		return true;
	}

	private void initialize() {
		mAgentTextView = (TextView) findViewById(R.id.agentText);
		mDownloadTextView = (TextView) findViewById(R.id.downloadText);
		mAgentChoice = mPreferences.getUserAgentChoice();
		mDownloadLocation = mPreferences.getDownloadDirectory();

		mDownloadTextView.setText(Constants.EXTERNAL_STORAGE + '/' + mDownloadLocation);

		switch (mAgentChoice) {
			case 1:
				mAgentTextView.setText(getResources().getString(R.string.agent_default));
				break;
			case 2:
				mAgentTextView.setText(getResources().getString(R.string.agent_desktop));
				break;
			case 3:
				mAgentTextView.setText(getResources().getString(R.string.agent_mobile));
				break;
			case 4:
				mAgentTextView.setText(getResources().getString(R.string.agent_custom));
		}

		RelativeLayout agent = (RelativeLayout) findViewById(R.id.layoutUserAgent);
		RelativeLayout download = (RelativeLayout) findViewById(R.id.layoutDownload);

		agent(agent);
		download(download);
	}

	public void agent(RelativeLayout view) {
		view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder agentPicker = new AlertDialog.Builder(mActivity);
                agentPicker.setTitle(getResources().getString(R.string.title_user_agent));
                mAgentChoice = mPreferences.getUserAgentChoice();
                agentPicker.setSingleChoiceItems(R.array.user_agent, mAgentChoice - 1,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mPreferences.setUserAgentChoice(which + 1);
                                switch (which + 1) {
                                    case 1:
                                        mAgentTextView.setText(getResources().getString(
                                                R.string.agent_default));
                                        break;
                                    case 2:
                                        mAgentTextView.setText(getResources().getString(
                                                R.string.agent_desktop));
                                        break;
                                    case 3:
                                        mAgentTextView.setText(getResources().getString(
                                                R.string.agent_mobile));
                                        break;
                                    case 4:
                                        mAgentTextView.setText(getResources().getString(
                                                R.string.agent_custom));
                                        agentPicker();
                                        break;
                                }
                            }
                        });
                agentPicker.setNeutralButton(getResources().getString(R.string.action_ok),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub

                            }

                        });
                agentPicker.setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        Log.i("Cancelled", "");
                    }
                });
                agentPicker.show();

            }

        });
	}

	public void agentPicker() {
		final AlertDialog.Builder agentStringPicker = new AlertDialog.Builder(mActivity);

		agentStringPicker.setTitle(getResources().getString(R.string.title_user_agent));
		final EditText getAgent = new EditText(this);
		agentStringPicker.setView(getAgent);
		agentStringPicker.setPositiveButton(getResources().getString(R.string.action_ok),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String text = getAgent.getText().toString();
						mPreferences.setUserAgentString(text);
						mAgentTextView.setText(getResources().getString(R.string.agent_custom));
					}
				});
		agentStringPicker.show();
	}

	public void download(RelativeLayout view) {
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder picker = new AlertDialog.Builder(mActivity);
				picker.setTitle(getResources().getString(R.string.title_download_location));
				mDownloadLocation = mPreferences.getDownloadDirectory();
				int n;
				if (mDownloadLocation.contains(Environment.DIRECTORY_DOWNLOADS)) {
					n = 1;
				} else {
					n = 2;
				}

				picker.setSingleChoiceItems(R.array.download_folder, n - 1,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

								switch (which + 1) {
									case 1:
										mPreferences
												.setDownloadDirectory(Environment.DIRECTORY_DOWNLOADS);
										mDownloadTextView.setText(Constants.EXTERNAL_STORAGE + '/'
												+ Environment.DIRECTORY_DOWNLOADS);
										break;
									case 2:
										downPicker();

										break;
								}
							}
						});
				picker.setNeutralButton(getResources().getString(R.string.action_ok),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

							}
						});
				picker.show();
			}

		});
	}

	@SuppressWarnings("deprecation")
	public void downPicker() {
		final AlertDialog.Builder downLocationPicker = new AlertDialog.Builder(mActivity);
		LinearLayout layout = new LinearLayout(this);
		downLocationPicker.setTitle(getResources().getString(R.string.title_download_location));
		final EditText getDownload = new EditText(this);
		getDownload.setBackgroundResource(0);
		mDownloadLocation = mPreferences.getDownloadDirectory();
		int padding = Utils.convertDpToPixels(10);

		LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

		getDownload.setLayoutParams(lparams);
		getDownload.setTextColor(Color.DKGRAY);
		getDownload.setText(mDownloadLocation);
		getDownload.setPadding(0, padding, padding, padding);

		TextView v = new TextView(this);
		v.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		v.setTextColor(Color.DKGRAY);
		v.setText(Constants.EXTERNAL_STORAGE + '/');
		v.setPadding(padding, padding, 0, padding);
		layout.addView(v);
		layout.addView(getDownload);
		if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			layout.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.edit_text));
		} else {
			layout.setBackground(getResources().getDrawable(android.R.drawable.edit_text));
		}
		downLocationPicker.setView(layout);
		downLocationPicker.setPositiveButton(getResources().getString(R.string.action_ok),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String text = getDownload.getText().toString();
						mPreferences.setDownloadDirectory(text);
						mDownloadTextView.setText(Constants.EXTERNAL_STORAGE + '/' + text);
					}
				});
		downLocationPicker.show();
	}

	@Override
	public void openAboutUsFragment() {

	}
}
