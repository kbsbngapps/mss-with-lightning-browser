/*
 * Copyright 2014 A.C.R. Development
 */
package com.kbsbng.androidapps.MultiSocialShare.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.Toolbar;

import com.kbsbng.androidapps.MultiSocialShare.R;

public class AboutSettingsActivity extends ThemableSettingsActivity implements OnClickListener {

	private int mEasterEggCounter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_settings);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//		setSupportActionBar(toolbar);
//		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setActionBar(toolbar);

		initialize();
	}

	@Override
	public String getAdmobAppId() {
		return null;
	}

	private void initialize() {

		RelativeLayout licenses = (RelativeLayout) findViewById(R.id.layoutLicense);
		RelativeLayout source = (RelativeLayout) findViewById(R.id.layoutSource);
		licenses.setOnClickListener(this);
		source.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.layoutLicense:
				// NOTE: In order to comply legally with open source licenses,
				// it is advised that you leave this code so that the License
				// Activity may be viewed by the user.
				startActivity(new Intent(this, LicenseActivity.class));
				break;
			case R.id.layoutSource:
				startActivity(new Intent(Intent.ACTION_VIEW,
						Uri.parse("http://twitter.com/MSocialShare"), this, MainActivity.class));
				finish();
				break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		finish();
		return true;
	}

	@Override
	public void openAboutUsFragment() {

	}
}
