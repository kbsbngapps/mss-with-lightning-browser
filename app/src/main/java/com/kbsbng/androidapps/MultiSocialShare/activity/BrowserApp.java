package com.kbsbng.androidapps.MultiSocialShare.activity;

import android.content.Context;

import com.kbsbng.androidapps.MultiSocialShare.R;
import com.kbsbng.androidapps.common.Application;

public class BrowserApp extends Application {

	public enum TrackerName {
		APP_TRACKER
	}

	private static Context context;

	@Override
	public int getTrackerXML() {
		return R.xml.analytics;
	}

	@Override
	public String getShortName() {
		return "calc";
	}

	@Override
	public String getFullName() {
		return "Calculator";
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();
	}

	public static Context getAppContext() {
		return context;
	}

}
