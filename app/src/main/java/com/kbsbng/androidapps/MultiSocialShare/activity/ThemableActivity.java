package com.kbsbng.androidapps.MultiSocialShare.activity;

import android.content.Intent;
import android.os.Bundle;

import com.kbsbng.androidapps.common.AppActivity;

import com.kbsbng.androidapps.MultiSocialShare.R;
import com.kbsbng.androidapps.MultiSocialShare.preference.PreferenceManager;

public abstract class ThemableActivity extends AppActivity {

	private boolean mDark;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mDark = PreferenceManager.getInstance().getUseDarkTheme();

		// set the theme
		if (mDark) {
			setTheme(R.style.Theme_DarkTheme);
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (PreferenceManager.getInstance().getUseDarkTheme() != mDark) {
			restart();
		}
	}

	protected void restart() {
		final Bundle outState = new Bundle();
		onSaveInstanceState(outState);
		final Intent intent = new Intent(this, getClass());
		finish();
		overridePendingTransition(0, 0);
		startActivity(intent);
	}
}
